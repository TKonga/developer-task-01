package com.econetwireless.epay.business.services.impl;

import com.econetwireless.epay.dao.requestpartner.api.RequestPartnerDao;
import com.econetwireless.epay.domain.RequestPartner;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Optional;

import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

public class PartnerCodeValidatorImpTest {

    @InjectMocks
    PartnerCodeValidatorImpl partnerCodeValidatorImpl;

    @Mock
    private RequestPartnerDao requestPartnerDao;

    private String partnerCode;
    @Before
    public void inti() {
        MockitoAnnotations.initMocks(this);
        partnerCode = "hot-recharge";
        partnerCodeValidatorImpl=new PartnerCodeValidatorImpl(requestPartnerDao);
    }

    @Test
    public void testValidatePartnerCodeImpl() {
        when(requestPartnerDao.findByCode(partnerCode)).thenReturn(new RequestPartner());
        partnerCodeValidatorImpl.validatePartnerCode(partnerCode);
        verify(requestPartnerDao, atLeastOnce()).findByCode(partnerCode);
    }

    @Test(expected=Exception.class)
    public void testValidatePartnerCodeImpl_throwsException() {
        partnerCodeValidatorImpl.validatePartnerCode(partnerCode);
        verify(requestPartnerDao, atLeastOnce()).findByCode(partnerCode);
    }
}
