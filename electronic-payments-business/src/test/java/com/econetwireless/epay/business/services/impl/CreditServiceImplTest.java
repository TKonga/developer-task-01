package com.econetwireless.epay.business.services.impl;

import com.econetwireless.epay.business.integrations.api.ChargingPlatform;
import com.econetwireless.epay.dao.subscriberrequest.api.SubscriberRequestDao;
import com.econetwireless.epay.domain.SubscriberRequest;
import com.econetwireless.utils.messages.AirtimeTopupRequest;
import com.econetwireless.utils.messages.AirtimeTopupResponse;
import com.econetwireless.utils.pojo.INCreditResponse;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;


public class CreditServiceImplTest {

    @InjectMocks
    private CreditsServiceImpl creditsService;
    @Mock
    ChargingPlatform chargingPlatform;
    @Mock
    SubscriberRequestDao subscriberRequestDao;

    private String partnerCode;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
        creditsService = new CreditsServiceImpl(chargingPlatform, subscriberRequestDao);
        partnerCode = "hot-recharge";

    }

    @Test
    public void testCreditMethod() {
        final AirtimeTopupRequest airtimeTopupRequest = new AirtimeTopupRequest();
        airtimeTopupRequest.setPartnerCode(partnerCode);
        airtimeTopupRequest.setReferenceNumber("REF-4654");
        airtimeTopupRequest.setAmount(2.75);
        airtimeTopupRequest.setMsisdn("774222654");
        Mockito.when(subscriberRequestDao.save((SubscriberRequest) any())).thenReturn(new SubscriberRequest());
        Mockito.when(chargingPlatform.creditSubscriberAccount(any())).thenReturn(new INCreditResponse());
        final AirtimeTopupResponse credit = creditsService.credit(airtimeTopupRequest);

        assertEquals(credit.getResponseCode(), null);
        assertEquals(credit.getBalance(), 0.0, 0);

    }

}
