package com.econetwireless.epay.business.utils;

import com.econetwireless.in.webservice.BalanceResponse;
import com.econetwireless.in.webservice.CreditRequest;
import com.econetwireless.in.webservice.CreditResponse;
import com.econetwireless.utils.pojo.INBalanceResponse;
import com.econetwireless.utils.pojo.INCreditRequest;
import com.econetwireless.utils.pojo.INCreditResponse;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

public class MessageConvertersTest {

    @InjectMocks
    MessageConverters messageConverters;

    @Before
    public void intMocks() throws ClassNotFoundException {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testConvertINCreditRequest() {
        INCreditRequest inCreditRequest = new INCreditRequest();
        assertNotNull(messageConverters.convert(inCreditRequest));
        inCreditRequest = null;
        assertNull(messageConverters.convert(inCreditRequest));

    }

    @Test
    public void testConvertCreditRequest() {
        CreditRequest creditRequest = new CreditRequest();
        assertNotNull(messageConverters.convert(creditRequest));
        creditRequest = null;
        assertNull(messageConverters.convert(creditRequest));
    }

    @Test
    public void testConvertINCreditResponse() {
        INCreditResponse increditResponse = new INCreditResponse();
        assertNotNull(messageConverters.convert(increditResponse));
        increditResponse = null;
        assertNull(messageConverters.convert(increditResponse));
    }

    @Test
    public void testConvertCreditResponse() {
        CreditResponse creditResponse = new CreditResponse();
        assertNotNull(messageConverters.convert(creditResponse));
        creditResponse = null;
        assertNull(messageConverters.convert(creditResponse));
    }

    @Test
    public void testConvertINBalanceResponse() {
        INBalanceResponse inbalanceResponse = new INBalanceResponse();
        assertNotNull(messageConverters.convert(inbalanceResponse));
        inbalanceResponse = null;
        assertNull(messageConverters.convert(inbalanceResponse));
    }

    @Test
    public void testConvertBalanceResponse() {
        BalanceResponse balanceResponse = new BalanceResponse();
        assertNotNull(messageConverters.convert(balanceResponse));
        balanceResponse = null;
        assertNull(messageConverters.convert(balanceResponse));
    }

}
