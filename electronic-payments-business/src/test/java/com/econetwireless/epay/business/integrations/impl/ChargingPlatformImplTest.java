package com.econetwireless.epay.business.integrations.impl;

import com.econetwireless.epay.business.utils.MessageConverters;
import com.econetwireless.in.webservice.BalanceResponse;
import com.econetwireless.in.webservice.CreditRequest;
import com.econetwireless.in.webservice.CreditResponse;
import com.econetwireless.in.webservice.IntelligentNetworkService;
import com.econetwireless.utils.pojo.INBalanceResponse;
import com.econetwireless.utils.pojo.INCreditRequest;
import com.econetwireless.utils.pojo.INCreditResponse;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.omg.PortableServer.POAPackage.ObjectAlreadyActive;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class ChargingPlatformImplTest {
    @InjectMocks
    ChargingPlatformImpl chargingPlatform;

    @Mock
    private IntelligentNetworkService intelligentNetworkService;

    private String partnerCode;
    private String msisdn;
    private INCreditRequest inCreditRequest;

    @Before
    public void init() throws ClassNotFoundException {
        MockitoAnnotations.initMocks(this);
        partnerCode = "hot-recharge";
        msisdn = "0777473783";
        inCreditRequest = new INCreditRequest();
    }

    @Test
    public void testEnquireBalance(){
        Mockito.when(intelligentNetworkService.enquireBalance(partnerCode, msisdn)).thenReturn(new BalanceResponse());
        final INBalanceResponse inBalanceResponse = chargingPlatform.enquireBalance(partnerCode, msisdn);
        assertNull(inBalanceResponse.getResponseCode());
    }

    @Test
    public void testCreditSubscriberAccount(){
        Mockito.when(intelligentNetworkService.creditSubscriberAccount(MessageConverters.convert(inCreditRequest)))
                .thenReturn(new CreditResponse());
        final INCreditResponse inCreditResponse = chargingPlatform.creditSubscriberAccount(inCreditRequest);
        //assertEquals(inCreditResponse.getBalance(), 0.0);
    }
}
