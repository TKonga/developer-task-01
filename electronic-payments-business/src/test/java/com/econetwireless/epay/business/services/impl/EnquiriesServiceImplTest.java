package com.econetwireless.epay.business.services.impl;

import com.econetwireless.epay.business.integrations.api.ChargingPlatform;
import com.econetwireless.epay.dao.subscriberrequest.api.SubscriberRequestDao;
import com.econetwireless.epay.domain.SubscriberRequest;
import com.econetwireless.utils.messages.AirtimeBalanceResponse;
import com.econetwireless.utils.pojo.INBalanceResponse;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;

public class EnquiriesServiceImplTest {

    @InjectMocks
    private EnquiriesServiceImpl enquiriesService;

    @Mock
    private ChargingPlatform chargingPlatform;
    @Mock
    private SubscriberRequestDao subscriberRequestDao;

    private String partnerCode;
    private String msisdn;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
        enquiriesService = new EnquiriesServiceImpl(chargingPlatform, subscriberRequestDao);
        partnerCode = "hot-recharge";
        msisdn = "774222654";
    }
    @Test
    public void testEnquiriesServiceImpl(){
        Mockito.when(subscriberRequestDao.save((SubscriberRequest) any())).thenReturn(new SubscriberRequest());
        Mockito.when(chargingPlatform.enquireBalance(partnerCode, msisdn)).thenReturn(new INBalanceResponse());
        final AirtimeBalanceResponse enquire = enquiriesService.enquire(partnerCode, msisdn);
        assertEquals(enquire.getResponseCode(), null);
    }
}
